# adapted from https://www.twilio.com/blog/asynchronous-http-requests-in-python-with-aiohttp

import asyncio
from time import perf_counter

import requests
from aiohttp import ClientSession


'''
Multithreading has the process rapidly shifting its attention between different threads
Asyncio lets you control this more directly using gather and await
Await means "I'm waiting for a value to come back, another thread can take over while I
wait.
Gather means "All of these awaitables should run in parallel"
'''


async def grab_pokemon(n: int, session: ClientSession):
    # Asynchronously perform a single request from the API
    url = f"https://pokeapi.co/api/v2/pokemon/{n}"
    async with session.get(url) as response:
        pokemon = await response.json()
        return pokemon


async def poke_range(lo: int, hi: int):
    # Creates an async generator of pokemon.
    async with ClientSession() as session:
        for i in range(lo, hi):
            pokemon = await grab_pokemon(i, session)
            yield pokemon


async def poke_bulk(lo: int, hi: int):
    # Asynchronously grabs all the pokemon at once.
    # Faster than getting all pokemon via async generator
    async with ClientSession() as session:
        tasks = [grab_pokemon(i, session) for i in range(lo, hi)]
        pokemon = await asyncio.gather(*tasks)
        return pokemon


def req_grab_pokemon(n: int):
    # Grab a pokemon synchronously
    url = f"https://pokeapi.co/api/v2/pokemon/{n}"
    response = requests.get(url)
    pokemon = response.json()
    return pokemon


def req_poke_range(lo: int, hi: int):
    # Same behavior as poke_range, but no parallelization, pokemon grabbed in series
    # Slower using async
    for i in range(lo, hi):
        pokemon = req_grab_pokemon(i)
        yield pokemon


async def async_main():
    async for pokemon in poke_range(1, 152):
        type_list = [t["type"]["name"] for t in pokemon["types"]]
        print(f"{pokemon['id']:03}: {pokemon['name']} ({', '.join(type_list)})")


async def async_bulk_main():
    pokemon_list = await poke_bulk(1, 152)
    for pokemon in pokemon_list:
        type_list = [t["type"]["name"] for t in pokemon["types"]]
        print(f"{pokemon['id']:03}: {pokemon['name']} ({', '.join(type_list)})")


def req_main():
    for pokemon in req_poke_range(1, 152):
        type_list = [t["type"]["name"] for t in pokemon["types"]]
        print(f"{pokemon['id']:03}: {pokemon['name']} ({', '.join(type_list)})")


if __name__ == "__main__":
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    start = perf_counter()
    # asyncio.run(async_main())
    asyncio.run(async_bulk_main())
    # req_main()
    end = perf_counter()
    print(f"{end-start:.4f} seconds")
