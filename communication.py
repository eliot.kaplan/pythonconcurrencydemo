from multiprocessing import Process, Queue, Lock
from typing import Callable, Any


'''
Simple demo of how to use multiprocessing queues to communicate between processes
'''


class MathProcess(Process):
    def __init__(self, f: Callable[[int], Any], print_lock: Lock):
        Process.__init__(self, daemon=True)
        self.input_queue = Queue()
        self.output_queue = Queue()
        self.f = f
        self.name = f.__name__
        self.print_lock = print_lock

    def run(self):
        while True:
            value = self.input_queue.get()
            if value < 0:
                break
            ans = self.f(value)
            with self.print_lock:
                print(f"{self.name}({value}) = {ans} (IN PROCESS)")
            self.output_queue.put((value, ans))


def factorial(n: int) -> int:
    return 1 if n == 0 else n * factorial(n - 1)


def fib(n: int) -> int:
    if n <= 1:
        return n
    return fib(n - 1) + fib(n - 2)


def collatz(n: int) -> int:
    c = 0
    while n != 1:
        c += 1
        n = n // 2 if n % 2 == 0 else 3 * n + 1
    return c


def factor(n: int) -> list[int]:
    out = []
    x = 2
    while n > 1:
        if n % x == 0:
            out.append(x)
            n //= x
        else:
            x += 1
    return out


def main():
    print_lock = Lock()
    processes = [
        MathProcess(func, print_lock) for func in (factorial, fib, collatz, factor)
    ]
    for process in processes:
        process.start()
    while True:
        i = input()
        value = -1 if len(i) == 0 else int(i)
        for process in processes:
            process.input_queue.put(value)
        if value < 0:
            break
    for process in processes:
        process.join()
    for process in processes:
        while not process.output_queue.empty():
            val, ans = process.output_queue.get()
            print(f"{process.name}({val}) = {ans} (MAIN)")


if __name__ == "__main__":
    main()
