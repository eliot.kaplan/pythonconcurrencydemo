import asyncio


'''asyncio can be used to create subprocesses. remember that these are still on the main
process, so if the subprocess is highly time intensive you'll want to put it on a
separate process instead'''


async def run(cmd: str):
    proc = await asyncio.create_subprocess_shell(
        cmd, stderr=asyncio.subprocess.PIPE, stdout=asyncio.subprocess.PIPE
    )

    stdout, stderr = await proc.communicate()

    print(f"[{cmd!r} exited with {proc.returncode}]")
    if stdout:
        print(f"[stdout]\n{stdout.decode()}")
    if stderr:
        print(f"[stderr]\n{stderr.decode()}")


def main():
    asyncio.run(run('ls -l'))


if __name__ == "__main__":
    main()
