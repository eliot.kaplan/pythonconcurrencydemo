from multiprocessing import Pool
from threading import Thread
from time import perf_counter


'''
Single processing and multithreading take about the same amount of time for CPU-bound
tasks. Multiprocessing is significantly faster because each task can run in parallel on
different CPU cores.

Remember:
- If you have multiple independent procedures that are CPU-intensive, parallelize them
  using multiprocessing
- If you have multiple independent procedures that have lots of waiting around time,
  parallelize them using threading. (for example UI or web requests)
'''


def fib(i: int) -> int:
    return i if i <= 1 else fib(i - 1) + fib(i - 2)


def fib_assigner(i: int, results: list[int]):
    results[i] = fib(i)


def multi_threaded(n: int):
    results = [0] * n
    threads = [Thread(target=fib_assigner, args=(i, results)) for i in range(n)]

    for i in range(n):
        threads[i].start()

    for i in range(n):
        threads[i].join()

    return results


def multi_processed(n: int):
    with Pool() as p:
        results = p.map(fib, range(n))
    return results


def single_processed(n: int):
    return [fib(i) for i in range(n)]


if __name__ == "__main__":
    start = perf_counter()
    single_processed(35)
    end = perf_counter()
    print(f"Single-processed took {end-start:.4f} seconds.")

    start = perf_counter()
    multi_threaded(35)
    end = perf_counter()
    print(f"Multi-threaded took {end-start:.4f} seconds.")

    start = perf_counter()
    multi_processed(35)
    end = perf_counter()
    print(f"Multi-processed took {end-start:.4f} seconds.")
